#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

void test_alloc(){
    heap_init(REGION_MIN_SIZE);
    void *a1 = _malloc(256);
    assert(a1);
    heap_term();
};
void test_free(){
    heap_init(REGION_MIN_SIZE);
    void *a1 = _malloc(256);
    void *a2 = _malloc(256);
    assert(a1 && a2);
    _free(a1);
    void *a3 = _malloc(256);
    assert(a3 && a1 == a3);
    heap_term();

}
void test_fatten(){
    heap_init(REGION_MIN_SIZE);
    void *a1 = _malloc(REGION_MIN_SIZE - 256);
    assert(a1);
    void *a2 = _malloc(2048);
    assert(a2);
    heap_term();
}
void test_nuhuh(){
    void *heap = heap_init(1024);
    assert(heap);
    void *a1 = _malloc(1024);
    assert(a1);
    void *bigone = mmap(HEAP_START + REGION_MIN_SIZE, 240, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void *a2 = _malloc(512);
    assert(a2);
    munmap(bigone, 1);

    _free(a2);
    _free(a1);
    heap_term();
}
int main()
{
    test_alloc();
    test_free();
    test_fatten();
    test_nuhuh();
    return 0;
}
